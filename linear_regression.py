#Justin Henn
#Project 2
#FS18-CMPSCI4340-E01

#imported libraries
import matplotlib.pyplot as plt
import numpy as np 

#regression function
def regression(x, y, w, eta):
    w_temp = [0] * 2

    m = len(y)
    for i in range(m):
        w_temp[0] += (1/m) * (((w[0] * x[i][0]) + (w[1] * x[i][1])) - y[i])
        w_temp[1] += (1/m) * x[i][1] * (((w[0] * x[i][0]) + (w[1] * x[i][1])) - y[i])
    w[0] = w[0] - (eta * w_temp[0])
    w[1] = w[1] - (eta * w_temp[1])


eta = .001
epochs = 20000
#data sets
x = [[1, 1],
     [1, 2],
     [1, 3],
     [1, 4],
     [1, 5],
     [1, 10],
     [1, 11],
     [1, 12],
     [1, 13]]
y = [0, 0, 0, 1, 1, 1, 1, 1, 1]

w = [0]*2

#run regression
for i in range(epochs):
    regression(x, y, w, eta)
print("w_0 = %.4f, w_1 = %.4f" % (w[0],w[1]))

#plot data
for i,z in enumerate(x):
    plt.scatter(x[i][1], y[i], s=120, linewidths=2)
x1 = np.linspace(-15, 15)
plt.plot(x1, ((w[1]*x1)+w[0])) 
